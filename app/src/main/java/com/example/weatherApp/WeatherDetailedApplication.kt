package com.example.weatherApp

import android.app.Application
import android.content.Context
import com.example.weatherApp.network.component.APIComponent
import com.example.weatherApp.network.component.APIModule
import com.example.weatherApp.network.component.DaggerAPIComponent
import com.example.weatherApp.network.repository.APIURL

class WeatherDetailedApplication : Application() {

    companion object {
        var context: Context? = null
        lateinit var apiComponent: APIComponent
    }
    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        apiComponent = initDaggerComponent()

    }

    fun getMyComponent(): APIComponent {
        return apiComponent
    }

    private fun initDaggerComponent(): APIComponent {
        apiComponent =  DaggerAPIComponent
            .builder()
            .aPIModule(
                APIModule(
                    APIURL.BASE_URL
                )
            )
            .build()
        return apiComponent

    }
}