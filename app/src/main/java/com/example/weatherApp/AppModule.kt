package com.example.weatherApp

import dagger.Module
import dagger.Provides

@Module
class AppModule constructor(weatherDetailedApplication: WeatherDetailedApplication){

    private var currentWeatherApplication = weatherDetailedApplication

    @Provides
    fun provideMyRetroApplication(): WeatherDetailedApplication {
        return currentWeatherApplication
    }
}