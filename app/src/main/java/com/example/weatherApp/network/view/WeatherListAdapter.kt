package com.example.weatherApp.network.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.tests.BR
import com.example.tests.R
import com.example.tests.databinding.WeatherDetailedItemBinding
import com.example.weatherApp.network.model.WeatherDetailed

class WeatherListAdapter(var context: Context) : RecyclerView.Adapter<WeatherListAdapter.ViewHolder>() {
    private  var list: List<WeatherDetailed> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val binding: WeatherDetailedItemBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.weather_detailed_item, parent, false)
        return ViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun setAdapterList(list: List<WeatherDetailed> ){
     this.list = list
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int = list.size

    class ViewHolder(val binding: WeatherDetailedItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Any) {
            binding.setVariable(BR.weatherdetailed, data)
            binding.executePendingBindings()
        }
    }
}
