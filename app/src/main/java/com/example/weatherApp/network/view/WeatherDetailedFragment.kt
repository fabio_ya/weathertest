package com.example.weatherApp.network.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tests.R
import com.example.tests.databinding.WeatherDetailedListLayoutBinding
import com.example.weatherApp.network.model.WeatherDetailed
import com.example.weatherApp.network.viewmodel.RetrofitViewModel
import com.example.weatherApp.network.viewmodel.RetrofitViewModelFactory
import kotlinx.android.synthetic.main.weather_detailed_list_layout.view.*

class WeatherDetailedFragment: Fragment() {

    lateinit var retrofitViewModel: RetrofitViewModel
    var fragmentView:View?=null
    private  var listAdapter: WeatherListAdapter?=null
    private var  weatherDetailedListLayoutBinding: WeatherDetailedListLayoutBinding?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        weatherDetailedListLayoutBinding = DataBindingUtil.inflate(inflater,R.layout.weather_detailed_list_layout,container,false)
        fragmentView = weatherDetailedListLayoutBinding?.root
        initAdapter()
        setAdapter()
        fetchWeatherInfo()
        return  fragmentView
    }

    private fun  initViewModel(){
        var retrofitViewModelFactory =
            RetrofitViewModelFactory()
        retrofitViewModel = ViewModelProviders.of(this,retrofitViewModelFactory).get(RetrofitViewModel::class.java)
    }

    fun fetchWeatherInfo(){
        retrofitViewModel.weatherDetailedLiveData?.observe(this,
            Observer<WeatherDetailed> { t ->
                t?.apply {
                    listAdapter?.setAdapterList(listOf(t))
                }
            })
    }

    private fun setAdapter(){
        fragmentView?.post_list?.apply {
            layoutManager = LinearLayoutManager(activity)
            addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
            adapter = listAdapter
        }

    }

   private fun initAdapter(){
       listAdapter =
           WeatherListAdapter(this@WeatherDetailedFragment.requireActivity())
   }

}
