package com.example.weatherApp.network.component

import com.example.weatherApp.AppModule
import com.example.weatherApp.network.repository.RetrofitRepository
import com.example.weatherApp.network.view.WeatherDetailedFragment
import com.example.weatherApp.network.viewmodel.RetrofitViewModel
import com.example.weatherApp.network.viewmodel.RetrofitViewModelFactory
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, APIModule::class])
interface APIComponent {
    fun inject(retrofitRepository: RetrofitRepository)
    fun inject(retrofitViewModel: RetrofitViewModel)
    fun inject(weatherDetailedFragment: WeatherDetailedFragment)
    fun inject(retrofitViewModelFactory: RetrofitViewModelFactory)
}