package com.example.weatherApp.network.repository

import androidx.lifecycle.MutableLiveData
import com.example.weatherApp.WeatherDetailedApplication
import com.example.weatherApp.network.component.APIComponent
import com.example.weatherApp.network.model.WeatherDetailed
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class RetrofitRepository {
    lateinit var apiComponent: APIComponent
    var weatherDetailedMutableLiveData: MutableLiveData<WeatherDetailed> = MutableLiveData()
    @Inject
    lateinit var retrofit: Retrofit

    constructor()

    init {
        var apiComponent : APIComponent =  WeatherDetailedApplication.apiComponent
        apiComponent.inject(this)
    }

    fun fetchWeatherDetailed(): MutableLiveData<WeatherDetailed> {

         var apiService: APIService = retrofit.create(
             APIService::class.java)
         var postListInfo : Call<WeatherDetailed> =  apiService.weatherDetailedRequest()
        postListInfo.enqueue(object :Callback<WeatherDetailed>{
            override fun onFailure(call: Call<WeatherDetailed>, t: Throwable) {
            }

            override fun onResponse(call: Call<WeatherDetailed>, response: Response<WeatherDetailed>) {
                var weatherResponse = response.body()
                weatherDetailedMutableLiveData.value = weatherResponse

            }
        })
         return weatherDetailedMutableLiveData

    }


}