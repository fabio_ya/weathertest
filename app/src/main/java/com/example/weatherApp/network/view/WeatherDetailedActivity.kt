package com.example.weatherApp.network.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import com.example.tests.R

class WeatherDetailedActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.weather_detailed_layout)
        replaceFragment()
    }

    private fun replaceFragment(){
       supportFragmentManager
           .beginTransaction()
           .replace(R.id.container_retro_room,
               WeatherDetailedFragment()
           )
           .commit()
    }
}
