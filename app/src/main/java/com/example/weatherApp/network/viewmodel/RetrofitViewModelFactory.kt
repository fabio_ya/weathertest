package com.example.weatherApp.network.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.weatherApp.WeatherDetailedApplication
import com.example.weatherApp.network.component.APIComponent
import com.example.weatherApp.network.repository.RetrofitRepository
import javax.inject.Inject

class RetrofitViewModelFactory : ViewModelProvider.Factory {
    @Inject
    lateinit var retrofitRepository: RetrofitRepository

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
       var apiComponent : APIComponent =  WeatherDetailedApplication.apiComponent
        apiComponent.inject(this)
        if (modelClass.isAssignableFrom(RetrofitViewModel::class.java)) {
            return RetrofitViewModel(
                retrofitRepository
            ) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}