package com.example.weatherApp.network.repository

import com.example.weatherApp.network.model.WeatherDetailed
import retrofit2.Call
import retrofit2.http.GET

interface APIService {
    @GET("data/2.5/forecast/daily?id=524901&appid=b1b15e88fa797225412429c1c50c122a1/:")
    fun weatherDetailedRequest(): Call<WeatherDetailed>
}