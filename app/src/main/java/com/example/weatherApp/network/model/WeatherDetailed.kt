package com.example.weatherApp.network.model

data class WeatherDetailed(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: List<WeatherItem>,
    val message: Int
)