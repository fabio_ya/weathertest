package com.example.weatherApp.network.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weatherApp.network.model.WeatherDetailed
import com.example.weatherApp.network.repository.RetrofitRepository

class RetrofitViewModel(var retrofitRepository: RetrofitRepository): ViewModel() {

    var weatherDetailedLiveData: LiveData<WeatherDetailed> = MutableLiveData()

    init {
        fetchFromRepository()
        }

    private fun fetchFromRepository(){
        weatherDetailedLiveData =  retrofitRepository.fetchWeatherDetailed()
    }


}