package com.example.tests

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.example.weatherApp.network.model.WeatherDetailed
import com.example.weatherApp.network.repository.RetrofitRepository
import com.example.weatherApp.network.viewmodel.RetrofitViewModel
import com.google.gson.Gson
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.io.FileReader

@RunWith(MockitoJUnitRunner::class)
class RetrofitViewModelTest {

   lateinit var retrofitViewModel: RetrofitViewModel
    @Mock
    lateinit var retrofitRepository: RetrofitRepository
    private lateinit var weatherDetailed: WeatherDetailed
    private val mockLiveData: MutableLiveData<WeatherDetailed> = MutableLiveData<WeatherDetailed>()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun init(){
        MockitoAnnotations.initMocks(this);
        this.retrofitViewModel = RetrofitViewModel(this.retrofitRepository)
        mockData()
    }

    @Test
    fun fetchweatherDetailedFromRepositoryTest(){
        retrofitViewModel.weatherDetailedLiveData = mockLiveData
        Assert.assertNotNull(retrofitViewModel.weatherDetailedLiveData.value)
        Assert.assertTrue(retrofitViewModel.weatherDetailedLiveData.value?.cod == "200")
        Assert.assertTrue(retrofitViewModel.weatherDetailedLiveData.value?.city?.name == "Moscow")

    }

    private fun mockData(){
        val gson = Gson()
        val weatherDetailed = gson.fromJson(FileReader("src/main/assets/weather_detailed.json"), WeatherDetailed::class.java)
        mockLiveData.postValue(weatherDetailed)
    }
}
