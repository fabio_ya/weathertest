# weatherTest
For this sample i used : 
       * **Dagger2 **
       * **DataBinding **
       * **Retrofit** 
       * **Mvvm**

*Dagger2

**Provider** provide dependencies is annotated as @Module and the methods that provides objects in this class to be annotated as @Provides.
**Consumer** is a class where we need to instantiate the objects. Dagger will provide the dependency, and you need to annotate @Inject the declaration of object.
**component** is an interface between provider and consumer. 

for example , for get weather detail with retrofit 

**APIMODULE.kt** is the dependency provider
**APIComponent.kt** is the component 
**RetrofitRepository.kt** is the consumer 

**WeatherApplication.kt** initialization of DaggerComponent to be accessible through the application in the Application context

`
private fun initDaggerComponent(): APIComponent {
        apiComponent =  DaggerAPIComponent
            .builder()
            .aPIModule(
                APIModule(
                    APIURL.BASE_URL
                )
            )
            .build()
        return apiComponent

}
`

**RetrofitRepository.kt** is the class that consumes dependency using @Inject annotation

` 
    @Inject
    lateinit var retrofit: Retrofit
    constructor()
    init {
        var apiComponent : APIComponent =  WeatherApplication.apiComponent
        apiComponent.inject(this)
    } 
`

**RetrofitViewModelFactory** is using to pass arguments to WeatherDetailedViewModel. In this program RetrofitRepository is injected in ViewModelProvider and sent as argument to RetrofitViewModel.

**APIURL** this class contain URL and service call interface

**APISERVICE**  is the class that contains service request 

I used **recycleView** to display any weather for different location in the main page. 

**WeatherListAdapter.kt** is using to map data to recycleView in WeatherDetailedFragment.

**WeatherDetailedFragment.kt** Observable must notify its callbacks whenever a change to the list occurs.

` 
fun fetchWeatherInfo(){
        retrofitViewModel.weatherDetailedLiveData?.observe(this,
            Observer<WeatherDetailed> { t ->
                t?.apply {
                    listAdapter?.setAdapterList(listOf(t))
                }
            })
} 

`

**dependencies using in this project**

`
dependencies {
    ext.dagger2_version = '2.24'
    implementation fileTree(include: ['*.jar'], dir: 'libs')
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    implementation 'androidx.appcompat:appcompat:1.0.0-beta01'
    implementation 'androidx.constraintlayout:constraintlayout:1.1.2'
    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'androidx.test:runner:1.1.0-alpha4'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.1.0-alpha4'
    implementation 'com.squareup.retrofit2:retrofit:2.6.1'
    implementation 'com.squareup.retrofit2:retrofit-converters:2.6.1'
    implementation 'com.squareup.retrofit2:retrofit-adapters:2.6.1'
    implementation 'com.squareup.retrofit2:converter-gson:2.6.1'
    // Basic Dagger 2 (required)
    implementation "com.google.dagger:dagger:$dagger2_version"
    kapt "com.google.dagger:dagger-compiler:$dagger2_version"
    // dagger.android package (optional)
    implementation "com.google.dagger:dagger-android:$dagger2_version"
    kapt "com.google.dagger:dagger-android-processor:$dagger2_version"
    // Support library support (optional)
    kapt "com.google.dagger:dagger-android-support:$dagger2_version"
    def lifecycle_version = "2.1.0"
    // ViewModel and LiveData
    implementation "androidx.lifecycle:lifecycle-extensions:$lifecycle_version"
    implementation 'androidx.appcompat:appcompat:1.1.0'
    implementation 'androidx.recyclerview:recyclerview:1.0.0'
    implementation 'androidx.cardview:cardview:1.0.0'
    implementation 'org.mockito:mockito-core:3.0.0'
    implementation 'org.mockito:mockito-inline:3.0.0'
    testImplementation 'android.arch.core:core-testing:1.1.1'
}
`
**Build.gradle**  Project configuration 
` 
compileSdkVersion 29
buildToolsVersion "29.0.2"
defaultConfig {
        applicationId "com.example.weatherApp"
        minSdkVersion 21
        targetSdkVersion 29
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
}
`




